import Home from '../views/Home';

export const _layout = '/dashboard';
export const _homepage = _layout + '/home';

export const brand = {
  name : 'BRAND',
  path : _homepage
};



//check available icons @ https://ant.design/components/icon/
export const routes = [
  {
    label : 'menu 1',
    icon : 'home',
    path : '/home',
    layout : _layout,
    component : Home,
    displayOnMenu : true, //SET TO  TRUE IF YOU WANT THIS ROUTE TO BE DISPLAYED ON SIDEBAR MENU
  },
  {
    label : 'menu 2',
    icon : 'dashboard',
    path : '/home2',
    layout : _layout,
    component : Home,
    displayOnMenu : true,
  }
]

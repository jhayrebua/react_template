import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Login from './views/Login';
import PageLayout from './layouts/PageLayout';

function App() {
  return (
    <Router>
      <Switch>
        <Provider store={store}>
          <Route path="/dashboard" render={props => <PageLayout {...props} />} />
          <Route path="/" render={props => <Login {...props} />} exact />
        </Provider>
      </Switch>
    </Router>
  );
}

export default App;

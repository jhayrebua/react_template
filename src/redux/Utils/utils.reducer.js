import { SET_LOADING, UNSET_LOADING } from './utils.type';


const initialState = {
  isFetching: false,
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        isFetching: true
      }
    case UNSET_LOADING:
      return {
        ...state,
        isFetching: false
      }
    default:
      return state;
  }
}
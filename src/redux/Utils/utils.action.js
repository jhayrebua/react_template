import { SET_LOADING, UNSET_LOADING } from './utils.type';

export const load = () => dispatch => {

  dispatch({ type: SET_LOADING });

}

export const unload = () => dispatch => {

  dispatch({ type: UNSET_LOADING });

}
import { GET_POSTS } from './posts.type';

const initialState = {
  posts: [],
  isLoaded: false
}

export default function Test(state = initialState, action) {
  switch (action.type) {
    case GET_POSTS:
      return {
        ...state,
        posts: action.payload.posts,
        isLoaded: true
      }
    default:
      return state;
  }
}
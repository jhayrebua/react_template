import { GET_POSTS } from './posts.type';
import { SET_LOADING, UNSET_LOADING } from '../Utils/utils.type';
import { headers, API, displayNotification, catchErrors } from '../../variables/Variables';
import axios from 'axios';

export const fetchPosts = () => dispatch => {
  dispatch({
    type: SET_LOADING
  })

  axios.get(API)
    .then(res => {

      setTimeout(() => {
        dispatch({
          type: GET_POSTS,
          payload: {
            posts: res.data
          }
        });

        dispatch({
          type: UNSET_LOADING
        })
      }, 4000);
    })
    .catch(err => {
      // displayNotification('error', err.message);
      catchErrors(err);
      dispatch({ type: UNSET_LOADING });
    })
}
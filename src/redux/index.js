import { combineReducers } from 'redux';
import Posts from './Posts/posts.reducer';
import Utils from './Utils/utils.reducer';

export default combineReducers({
  Posts,
  Utils
});

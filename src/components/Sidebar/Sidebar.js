import React from 'react';
import { Icon } from 'antd';
import { Link } from 'react-router-dom';

const Sidebar = props => {
  const { isHidden, brand, routes, pathname } = props;

  const menu_item = routes.map((props, key) => {
    if (!props.displayOnMenu)
      return false;

    const _path = props.layout + props.path;
    return (
      <li key={key}>
        <Link className={pathname === _path ? 'active-link' : null} to={_path}>
          <Icon type={props.icon} />
          <span>{props.label}</span>
        </Link>
      </li>
    )
  })

  return (
    <div className={!isHidden ? "sidebar" : "sidebar sidebar-hidden"}>
      <ul>
        <Link to={brand.path} className='brand'>
          {brand.name}
        </Link>
        <hr className="sidebar-divider" />
        {menu_item}
      </ul>
    </div>
  )
}

export default Sidebar

import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Formik, Field, Form } from 'formik';
import { Button, notification } from 'antd';
import { AntInput, AntPassword } from '../Antfield';
import { _homepage } from '../../variables/routes';
import { API, setToken } from '../../variables/Variables';
import { SET_LOADING, UNSET_LOADING } from '../../redux/Utils/utils.type';
import * as Yup from 'yup';
import axios from 'axios';

import Loader from '../Loader/Loader';
const defaultValues = {
  username: '',
  password: '',
}

const SubmitHandler = (values, setSubmitting, setValues, submitCount, addSubmitCount) => dispatch => {

  if (submitCount[values.username] && submitCount[values.username] > 2) {
    notification.error({
      message: 'You have tried 3 times'
    });
    setSubmitting(false);

    if (submitCount[values.username] === 3) {

      setTimeout(() => {
        delete submitCount[values.username];
        addSubmitCount(submitCount);
      }, 120000);
    }
    return false;
  }
  dispatch({
    type: SET_LOADING
  })
  axios.post(API + "login", values)

    .then(res => {

      setToken(res.data.access_token);
      window.location.href = _homepage;

      setSubmitting(false);
      dispatch({
        type: UNSET_LOADING
      })
    })
    .catch(err => {

      if (err.response) {
        notification.error({
          message: 'Invalid username or password'
        });
        values.password = '';
        setValues(values);

        if (submitCount[values.username]) {
          submitCount[values.username] += 1;
          addSubmitCount(submitCount);
        } else {
          submitCount[values.username] = 1;
          addSubmitCount(submitCount);
        }

      }

      dispatch({
        type: UNSET_LOADING
      })
      setSubmitting(false);
    })

}

const validate = Yup.object().shape({
  username: Yup.string()
    .required('Username is required')
    .min(2, 'Username should be atleast 2 characters')
    .max(12, 'Username should not exceed 12 characters'),
  password: Yup.string()
    .required('Password is required')
    .min(3, 'Password should be atleast 3 characters')
    .max(12, 'Password should not exceed 12 characters'),
})

function LoginForm(props) {

  const { dispatch, isFetching } = props;
  const [submitCount, addSubmitCount] = useState({});
  return (
    <Formik
      validationSchema={validate}
      onSubmit={(values, { setSubmitting, setValues }) => SubmitHandler(values, setSubmitting, setValues, submitCount, addSubmitCount)(dispatch)}
      initialValues={defaultValues}>
      {({ values, isSubmitting }) => {

        return (
          <Form className="form-login">
            {
              isFetching ?
                <Loader />
                : null
            }
            <span className="form-title">ACCOUNT LOGIN</span>
            <Field
              component={AntInput}
              name="username"
              value={values.username}
              autoComplete="off"
              size="large"
              placeholder="Username"
            />

            <Field
              component={AntPassword}
              name="password"
              value={values.password}
              autoComplete="off"
              size="large"
              placeholder="Password"
            />
            <Button size="large" htmlType="submit" disabled={isSubmitting} block>SIGN IN</Button>
          </Form>
        )

      }}
    </Formik>
  )
}

const mapStateToProps = state => ({
  isFetching: state.Posts.isFetching
})

export default connect(mapStateToProps, null)(LoginForm)

import React, { Component } from 'react';
import Title from '../components/Title/Title';
import { fetchPosts } from '../redux/Posts/posts.action';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Card } from 'antd';

export class Home extends Component {
  componentDidMount() {
    this.props.fetchPosts();
  }

  shouldComponentUpdate(nextProps) {

    if (nextProps.posts.length > 0)
      return true;
    else
      return false;

  }

  render() {
    const { label, location: { pathname }, isMenuHidden, posts } = this.props;

    const displayPosts = posts.map((posts, i) => (
      <Card title={posts.title} key={i} style={{ marginBottom : 5 }}>
        {posts.body}
      </Card>
    ))

    return (
      <div className={isMenuHidden ? "content content-hidden" : "content"}>
        <Title pathname={pathname} label={label} />

        <div className="wrapper">
          {displayPosts}
        </div>

      </div>
    )
  }
}

Home.propTypes = {
  fetchPosts: PropTypes.func.isRequired
}

const mapStateToProps = state => {

  return {
    posts: state.Posts.posts
  }

}

export default connect(mapStateToProps, { fetchPosts })(Home);
